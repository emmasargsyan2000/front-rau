import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AuthenticationService} from "./services/authentication.service";
import {CookieService} from "./services/cookie.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoginGuard} from "./login/login-guard.service";
import {LoginPagePreventionGuard} from "./login/login-page-prevention-guard.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthenticationService,
    CookieService,
    LoginGuard,
    LoginPagePreventionGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
