import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginPagePreventionGuard} from "./login/login-page-prevention-guard.service";
import {LoginGuard} from "./login/login-guard.service";

const routes: Routes = [
  {
    path: 'login',
    canActivate: [LoginPagePreventionGuard],
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule)
  },
  {
    path: '',
  //  canLoad: [LoginGuard],
    loadChildren: () => import('./main/main.module').then(m => m.MainModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
