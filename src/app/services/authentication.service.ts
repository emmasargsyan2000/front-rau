import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import {Injectable, OnDestroy} from '@angular/core';
import {
  filter,
  first,
  map,
  mergeMap,
  switchMap,
  takeUntil
} from 'rxjs/operators';
import {identity, noop, Observable, of, ReplaySubject} from 'rxjs';
import {Subject} from 'rxjs';
import {CookieService} from "./cookie.service";

/**
 * Http implementation of AuthenticationService
 */
@Injectable()
export class AuthenticationService implements OnDestroy {

  public static OAUTH_SERVICE_TOKEN_ENDPOINT = '/oauth/token';
  public static OAUTH_SERVICE_CHECK_TOKEN_ENDPOINT = '/oauth/check_token';
  public static IS_SAML_USER = 'is_saml_user';
  public static OAUTH_SERVICE_USERDATA_ENDPOINT = '/me';
  public static serviceUrl = "localhost:8084"
  public static OAUTH_SERVICE_LOGOUT_ENDPOINT = '/logout';
  public static CLIENT_SECRET = 'secret';
  public static API_KEY_CLIENT_ID = 'api_key_client';
  public static TOKEN_KEY_NAME = 'access_token';
  public static USERNAME_KEY_NAME = 'username';
  public static CSRF_TOKEN_KEY_NAME = 'csrfToken';

  public static getInstance(http: HttpClient, cookieService: CookieService, // tslint:disable-next-line:no-any
    _?: any): AuthenticationService {
    if (this._instance === undefined) {
      this._instance = new AuthenticationService(
        http,
        cookieService
      );
    }
    return this._instance;
  }

  protected static _instance: AuthenticationService | undefined = undefined;
  public redirectUrl = '';
  protected loggedOn: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  protected userData = new ReplaySubject<UserData>(1);
  protected destroySubject$: Subject<void> = new Subject<void>();
  private username: string;

  constructor(
    protected http: HttpClient,
    protected cookieService: CookieService
  ) {
    this.username = this.getUsername() || '';
    this.checkToken();
  }

  public ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }

  /**
   * @returns current user data
   */
  public getUserData(): Observable<UserData> {
    return this.userData.asObservable();
  }
  /**
   * @returns true if user is logged on
   */
  public isLoggedOn(): Observable<boolean> {
    return this.loggedOn.asObservable();
  }

  /**
   * Login User
   */
  public login(
    username: string,
    password: string
  ): Observable<any> {
    const httpParams = new HttpParams()
      .set('grant_type', 'password')
      .set('username', username)
      .set('password', password);
    this.setUsername(username);
    return this.loginWithHeaders(
      httpParams
    );
  }

  protected loginWithHeaders(
    httpParams: HttpParams
  ): Observable<any> {
    const options = this.oauthClientHeaders();
    return this.http.post(
        AuthenticationService.serviceUrl + AuthenticationService.OAUTH_SERVICE_TOKEN_ENDPOINT,
        httpParams,
        options
      )
      .pipe(
        switchMap((response: HttpResponse<Object>) => {
          if (response.status === 200) {
            const body: object = response.body as any;
            while (
              this.cookieService.check(
                AuthenticationService.TOKEN_KEY_NAME
              )
              ) {
              this.cookieService.delete(
                AuthenticationService.TOKEN_KEY_NAME,
                '/'
              );
            }
            // @ts-ignore
            this.cookieService.set(AuthenticationService.TOKEN_KEY_NAME, body['access_token'],
              undefined, '/');
            return this.userDataObservable().pipe(
              map(() => {
                this.loggedOn.next(true);
                return {
                  loginResponse: 0
                }
              })
            );
          } else {
            return of(
              {
                loginResponse: 1
              }
            );
          }
        })
      );
  }


  protected getUserToLogin(): string | null{
    return localStorage.getItem('userToLogin');
  }

  protected setUserToLogin(userToLogin: string): void {
    return localStorage.setItem('userToLogin', userToLogin);
  }

  /**
   * @inheritDoc
   */
  public logout(withoutRequest: boolean = false): void {
    if (!withoutRequest) {
        this.http.get(
          AuthenticationService.serviceUrl + AuthenticationService.OAUTH_SERVICE_LOGOUT_ENDPOINT
        ).pipe(
          map(() => {
            this.cookieService.delete(
              AuthenticationService.IS_SAML_USER,
              '/'
            );
            this.cookieService.delete(
              AuthenticationService.TOKEN_KEY_NAME,
              '/'
            );
            localStorage.removeItem(
              AuthenticationService.CSRF_TOKEN_KEY_NAME
            );
          }),
          first()
        )
        .subscribe(noop);
    }
    this.loggedOn.next(false);
  }

  public checkToken(): void {
    this.checkTokenValidity();
    this.loggedOn
      .pipe(
        filter(identity),
        mergeMap(() => this.userDataObservable()),
        takeUntil(this.destroySubject$)
      )
      .subscribe(noop, err => console.error(err));
  }

  protected checkTokenValidity(): void {
    let httpParams = new HttpParams();
    const accessToken = this.cookieService.get(AuthenticationService.TOKEN_KEY_NAME);
    if (accessToken) {
      httpParams = httpParams.set('token', accessToken);
    }
    const options = this.oauthClientHeaders();
      this.http.post(
        AuthenticationService.serviceUrl + AuthenticationService.OAUTH_SERVICE_CHECK_TOKEN_ENDPOINT,
        httpParams,
        options
      ).pipe(
        takeUntil(this.destroySubject$)
      )
      .subscribe(
        (response: HttpResponse<object>) => {
          this.loggedOn.next(true);
        },
        () => {
          if (accessToken) {
            this.cookieService.delete(AuthenticationService.TOKEN_KEY_NAME);
          }
          this.loggedOn.next(false);
        }
      );
  }

  protected userDataObservable(): Observable<void> {
    return this.http.get(
      AuthenticationService.serviceUrl + AuthenticationService.OAUTH_SERVICE_USERDATA_ENDPOINT,
        {
          observe: 'response',
        }
        ).pipe(
          map((resp: HttpResponse<object>) => {
            const userData: UserData = new UserData();
            if (resp.status === 200) {
              const data = resp.body as any;
              userData.fullName = data['userName'];
              userData.userId = Number(data['userId']);
              userData.username = this.getUsername() || '';
            }
            this.userData.next(userData);
          })
    );
  }

  protected oauthClientHeaders(): {
    headers: HttpHeaders;
    observe: 'response';
  } {
    const encoded = btoa(AuthenticationService.API_KEY_CLIENT_ID + ':' + AuthenticationService.CLIENT_SECRET);
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic ' + encoded)
      .append(
        'Content-Type',
        'application/x-www-form-urlencoded; charset=utf-8'
      );
    return {headers: headers, observe: 'response'};
  }

  protected setUsername(username: string): void {
    this.username = username;
    localStorage.setItem(AuthenticationService.USERNAME_KEY_NAME, username);
  }

  protected getUsername(): string | null {
    return (
      this.username ||
      localStorage.getItem(AuthenticationService.USERNAME_KEY_NAME)
    );
  }

}


export class UserData {
  public fullName?: string;
  public userId?: number;
  public username?: string;
}
