import {DOCUMENT} from '@angular/common';
import {Inject, Injectable} from '@angular/core';


@Injectable()
export class CookieService {
  /**
   * @param name Cookie name
   * @returns {RegExp}
   */
  private static getCookieRegExp(name: string): RegExp {
    const escapedName = name.replace(/([\[\]{}()|=;+?,.*^$])/gi, '\\$1');
    return new RegExp(
      '(?:^' + escapedName + '|;\\s*' + escapedName + ')=(.*?)(?:;|$)',
      'g'
    );
  }
  private readonly document: Document;
  // tslint:disable-next-line:no-any
  constructor(@Inject(DOCUMENT) document: any) {
    this.document = document;
  }
  /**
   * @param name Cookie name
   * @returns {boolean}
   */
  public check(name: string): boolean {
    if (this.document === undefined) {
      return false;
    }
    name = encodeURIComponent(name);
    return CookieService.getCookieRegExp(name).test(this.document.cookie);
  }
  /**
   * @param name Cookie name
   * @returns {string}
   */
  public get(name: string): string {
    if (this.document === undefined || !this.check(name)) {
      return '';
    }
    // @ts-ignore
    return decodeURIComponent(CookieService.getCookieRegExp(encodeURIComponent(name)).exec(this.document.cookie)[1]);
  }
  public getAll(): object {
    if (this.document === undefined) {
      return {};
    }
    const cookies: {} = {};
    if (this.document.cookie) {
      this.document.cookie.split(';').forEach(cookie => {
        const [key, value] = cookie.split('=');
        // @ts-ignore
        cookies[decodeURIComponent(key.replace(/^ /, ''))] = decodeURIComponent(value);
      });
    }
    return cookies;
  }
  /**
   * @param name    Cookie name
   * @param value   Cookie value
   * @param expires Number of days until the cookies expires or an actual `Date`
   * @param path    Cookie path
   * @param domain  Cookie domain
   * @param secure  Secure flag
   */
  public set(
    name: string,
    value: string,
    expires?: number | Date,
    path?: string,
    domain?: string,
    secure?: boolean
  ): void {
    if (this.document === undefined) {
      return;
    }
    let cookieString: string =
      encodeURIComponent(name) + '=' + encodeURIComponent(value) + ';';
    if (expires) {
      if (typeof expires === 'number') {
        const dateExpires: Date = new Date(
          new Date().getTime() + expires * 1000 * 60 * 60 * 24
        );
        cookieString += 'expires=' + dateExpires.toUTCString() + ';';
      } else {
        cookieString += 'expires=' + expires.toUTCString() + ';';
      }
    }
    if (path) {
      cookieString += 'path=' + path + ';';
    }
    if (domain) {
      cookieString += 'domain=' + domain + ';';
    }
    if (secure) {
      cookieString += 'secure;';
    }
    this.document.cookie = cookieString;
  }
  /**
   * @param name   Cookie name
   * @param path   Cookie path
   * @param domain Cookie domain
   */
  public delete(name: string, path?: string, domain?: string): void {
    if (this.document === undefined) {
      return;
    }
    this.set(name, '', -1, path, domain);
  }

  /**
   * @param path   Cookie path
   * @param domain Cookie domain
   */
  public deleteAll(path?: string, domain?: string): void {
    if (this.document === undefined) {
      return;
    }
    const cookies = this.getAll();
    for (const cookieName in cookies) {
      if (cookies.hasOwnProperty(cookieName)) {
        this.delete(cookieName, path, domain);
      }
    }
  }

}
