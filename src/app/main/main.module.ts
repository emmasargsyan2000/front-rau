import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BookListComponent} from "./book-list/book-list.component";
import {MainRoutingModule} from "./main-routing.module";
import { MainComponent } from './main.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {UserListComponent} from "./user-list/user-list.component";
import {ReportsComponent} from "./reports/reports.component";
import {BorrowedBooksComponent} from "./borrowed-books/borrowed-books.component";
import { GoogleChartsModule } from 'angular-google-charts';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbHighlight, NgbPaginationModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {BookService} from "./services/book.service";
import {UserService} from "./services/user.service";
import {ReportsService} from "./services/reports.service";
import {BookDetailComponent} from "./book-detail/book-detail.component";
import {MatGridListModule} from "@angular/material/grid-list";
import {DialogComponent} from "./dialog/dialog.component";
import {MatDialogModule} from "@angular/material/dialog";
import {HomeComponent} from "./home/home.component";
import {MatSortModule} from "@angular/material/sort";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCardModule} from "@angular/material/card";
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
  declarations: [
    BookListComponent,
    MainComponent,
    UserListComponent,
    ReportsComponent,
    BorrowedBooksComponent,
    BookDetailComponent,
    DialogComponent,
    HomeComponent
  ],
  providers: [
    BookService,
    UserService,
    ReportsService,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    GoogleChartsModule,
    ReactiveFormsModule,
    FormsModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    MatGridListModule,
    MatDialogModule,
    MatSortModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatTooltipModule,
  ]
})
export class MainModule { }
