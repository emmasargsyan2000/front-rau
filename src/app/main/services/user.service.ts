import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable()
export class UserService {

  //todo

  constructor() { }

  getUsers(): Observable<User[]>{
    return new Observable((observer) => observer.next(users));
  }

}

export interface User {
  name: string;
  faculty: string;
  year: number;
  type: string;
}

const users: User[] = [
    {
      name: "Artur Sargsyan",
      faculty: "IH",
      year: 2022,
      type: "Student"
    },
    {
      name: "Levon Avetisyan",
      "faculty": "ILP",
      "year": 2021,
      "type": "Student"
    },
    {
      name: "Anait Karapetyan",
      "faculty": "IH",
      "year": 2021,
      "type": "Student"
    },
    {
      name: "Gagik Torosyan",
      "faculty": "IMAC",
      "year": 2019,
      "type": "Student"
    },
    {
      name: "Naira Movsesyan",
      "faculty": "IBP",
      "year": 2023,
      "type": "Student"
    },
    {
      name: "Alla Saakyan",
      "faculty": "IMAC",
      "year": 2020,
      "type": "Student"
    },
    {
      name: "Tatevik Minasyan",
      "faculty": "IMAC",
      "year": 2023,
      "type": "Student"
    }
];
