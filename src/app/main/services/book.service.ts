import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable()
export class BookService {

  //todo

  constructor() { }

  getBooks(): Observable<Book[]>{
    return new Observable((observer) => observer.next(books));
  }

  getBookById(bookId: number): Observable<Book>{
    if(bookId == -1){
      return new Observable((observer) => observer.next({
        author: "",
        category: ""
      } as Book));
    }
    return new Observable((observer) => observer.next(books.find(book => book.id === bookId)));
  }

  updateBookById(book: any, bookId: number): Observable<any>{
    books[books.findIndex(b => b.id === bookId)] = book;
    return new Observable((observer) => observer.next({}));
  }

  createBookById(book: any): Observable<any>{
    books.push(book);
    return new Observable((observer) => observer.next({}));
  }

  getUserBooks(userId: number): Observable<BorrowedBook[]>{
    return new Observable((observer) => observer.next(borrowedBook));
  }

  borrowBook(bookId: number, userId: number): Observable<boolean>{
    return new Observable((observer) => observer.next(true));
  }

  getAuthors(): Observable<Author[]>{
    return new Observable((observer) => observer.next(authors));
  }

  getGenres(): Observable<Genre[]>{
    return new Observable((observer) => observer.next(genres));
  }

}

export interface Genre {
  id: number;
  name: string;
}

export interface Author {
  id: number;
  name: string;
}

export interface Book {
  id: number;
  name: string;
  author: string;
  category: string;
  rate: number;
  image: string;
}

export interface BorrowedBook {
  name: string;
  author: string;
  category: string;
  startDate: any;
  endDate: any;
}

const authors: Author[] = [
  {
    id: 1,
    name: 'Author 1'
  },
  {
    id: 2,
    name: 'Author 2',
  }
];

const genres: Genre[] = [
  {
    id: 1,
    name: 'Genre 1'
  },
  {
    id: 2,
    name: 'Genre 2',
  }
];

const books: Book[] = [
    {
      id: 1,
      "name": "100 Years of Solitude",
      "author": "G. G. Márquez",
      "category": "Novel",
      "image": "/assets/100_years_of_solitude_cover.jpg",
      rate: 4.5
    },
    {
      id: 2,
      "name": "Anna Karenina",
      "author": "L. Tolstoy",
      "category": "Novel",
      "image": "/assets/anna_karenina_cover.jpg",
      rate: 4.4
    },
    {
      id: 3,
      "name": "The Great Gatsby",
      "author": "F. Scott Fitzgerald",
      "category": "Novel",
      "image": "/assets/the_great_gatsby_cover.jpg",
      rate: 4.1
    },
    {
      id: 4,
      "name": "The Godfather",
      "author": "Mario Puzo",
      "category": "Novel",
      "image": "/assets/the_godfather_cover.jpg",
      rate: 3.9
    },
    {
      id: 5,
      "name": "An American Tragedy",
      "author": "Theodore Dreiser",
      "category": "Novel",
      "image": "/assets/an_american_tragedy_cover.jpg",
      rate: 4.2
    },
    {
      id: 6,
      "name": "1984",
      "author": "George Orwell",
      "category": "Dystopian",
      "image": "/assets/1984_cover.jpg",
      rate: 5
    },
    {
      id: 7,
      "name": "Why Integration Marketing",
      "author": "Cassie Hubbell",
      "category": "Marketing",
      "image": "/assets/why_integration_marketing_cover.jpg",
      rate: 3
    },
    {
      id: 8,
      "name": "A Brief History of Time",
      "author": "Stephen Hawking",
      "category": "Science",
      "image": "/assets/a_brief_history_of_time_cover.jpg",
      rate: 3.6
    },
    {
      id: 9,
      "name": "Three Comrades",
      "author": "Erich Maria Remarque",
      "category": "Novel",
      "image": "/assets/three_comrades_cover.jpg",
      rate: 4
    },
    {
      id: 10,
      "name": "Lost and Founder",
      "author": "Rand Fishkin",
      "category": "Business",
      "image": "/assets/lost_and_founder_cover.jpg",
      rate: 4.8
    },
    {
      id: 19,
      "name": "Macbeth",
      "author": "William Shakespeare",
      "category": "Tragedy",
      "image": "/assets/macbeth_cover.jpg",
      rate: 4.9
    }
];

const borrowedBook: BorrowedBook[] = [
  {
    "name": "The Godfather",
    "author": "M. Puzo",
    "category": "Novel",
    "startDate": new Date(2024, 5, 10),
    "endDate": new Date(2024, 11, 10)
  },
  {
    "name": "Java. The Complete Guide",
    "author": "H. Schildt",
    "category": "Education",
    "startDate": new Date(2024, 7, 9),
    "endDate": new Date(2025, 0, 9)
  },
  {
    "name": "War and Peace",
    "author": "L. Tolstoy",
    "category": "Novel",
    "startDate": new Date(2024, 4, 15),
    "endDate": new Date(2025, 1, 15)
  },
  {
    "name": "Romeo and Juliet",
    "author": "W. Shakespeare",
    "category": "Tragedy",
    "startDate": new Date(2024, 2, 10),
    "endDate": new Date(2024, 4, 1)
  }

];
