import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable()
export class ReportsService {

  //todo

  constructor() { }

  getNumOfBooksByFaculty(): Observable<any[]>{
    return new Observable((observer) => observer.next([
      ['Firefox', 45.0],
      ['IE', 26.8],
      ['Chrome', 12.8],
      ['Safari', 8.5],
      ['Opera', 6.2],
      ['Others', 0.7]
    ]));
  }

  getNumOfBooksByGenre(): Observable<any[]> {
    return new Observable((observer) => observer.next( [
      ['Firefox', 45.0],
      ['IE', 26.8],
      ['Chrome', 12.8],
      ['Safari', 8.5],
      ['Opera', 6.2],
      ['Others', 0.7]
    ]));
  }

}
