import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {Observable, startWith} from "rxjs";
import {FormControl} from "@angular/forms";
import {map} from "rxjs/operators";
import {User, UserService} from "../services/user.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements AfterViewInit {

  formControl = new FormControl('');
  dataSource!: MatTableDataSource<User>;

  users$!: Observable<User[]>;
  users: User[] = [];
  collectionSize = this.users.length;
  filter = new FormControl('', { nonNullable: true });
  page = 1;
  pageSize = 10;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  displayedColumns = [ 'name', 'faculty', 'year', 'type'];

  applyFilter(filterValue: any) {
    filterValue = filterValue.target.value.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  constructor(private userService: UserService) {
    this.userService.getUsers().subscribe((users: User[]) => {
      this.users = users;
      this.dataSource = new MatTableDataSource(users);
      this.collectionSize = users.length;
      this.users$ = this.filter.valueChanges.pipe(
        startWith(''),
        map((text) => search(this.users, text, this.page, this.pageSize)),
      );
    });
  }

  refreshCountries() {
    this.users$ = this.filter.valueChanges.pipe(
      startWith(''),
      map((text) => search(this.users, text, this.page, this.pageSize)),
    );
  }

}

function search(users: User[], text: string, page: number, pageSize: number): User[] {
  return users.filter((user) => {
    const term = text.toLowerCase();
    return (
      user.name.toLowerCase().includes(term)
    );
  }).slice(
    (page - 1) * pageSize, (page - 1) * pageSize + pageSize,
  );
}
