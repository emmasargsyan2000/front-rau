import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Author, Book, BookService, Genre} from "../services/book.service";
import {map, switchMap} from "rxjs/operators";
import {MatTableDataSource} from "@angular/material/table";
import {startWith} from "rxjs";

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  constructor( private route: ActivatedRoute,
               private router: Router,
               private bookService: BookService) { }

  public book!: Book;
  public bookId!: number;
  public genres!: Genre[];
  public authors!: Author[];
  trendingBooks: Book[] = [];
  public isNew = true;
  public isQR = false;

  addQR(){
    this.isQR = true;
  }
  ngOnInit(): void {
    this.bookService.getBooks().subscribe((books: Book[]) => {
      this.trendingBooks = books.sort((a, b) => b.rate - a.rate ).slice(0, 5);
    });

    this.route.paramMap.pipe(switchMap((params: ParamMap) =>{
      this.isNew = Number(params.get('id')!) === -1;
      return this.bookService.getBookById(Number(params.get('id')!))
    }
    )).subscribe(book => {
      this.book = book;
      this.bookId = book.id;
    });
    this.bookService.getAuthors().subscribe(authors => this.authors = authors);
    this.bookService.getGenres().subscribe(genres => this.genres = genres);
  }

  showIcon(rating:number) {
    return [].constructor( Math.floor(rating))
  }

  openBook(bookId: number){
    this.router.navigate(['/books', bookId]);
  }
  addBook() {
    (this.bookId ? this.bookService.updateBookById(this.book, this.bookId) : this.bookService.createBookById(this.book))
      .subscribe(() => this.router.navigate(['/books']));
  }

}
