import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Observable, startWith} from "rxjs";
import {Book, BookService} from "../services/book.service";
import {FormControl} from "@angular/forms";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "../dialog/dialog.component";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements AfterViewInit {
  dataSource!: MatTableDataSource<Book>;

  books$!: Observable<Book[]>;
  books: Book[] = [];
  trendingBooks: Book[] = [];
  collectionSize = this.books.length;
  filter = new FormControl('', { nonNullable: true });
  page = 1;
  pageSize = 5;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  applyFilter(filterValue: any) {
    filterValue = filterValue.target.value.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  displayedColumns = [ 'name', 'author', 'category'];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private bookService: BookService
  ) {

    this.bookService.getBooks().subscribe((books: Book[]) => {
      this.books = books;
      this.trendingBooks = books.sort((a, b) => b.rate - a.rate ).slice(0, 5);
      this.dataSource = new MatTableDataSource(books);
      this.collectionSize = books.length;
      this.books$ = this.filter.valueChanges.pipe(
        startWith(''),
        map((text) => search(this.books, text, this.page, this.pageSize)),
      );
    });
  }


  refreshCountries() {
    this.books$ = this.filter.valueChanges.pipe(
      startWith(''),
      map((text) => search(this.books, text, this.page, this.pageSize)),
    );
  }

  createBook(){
    this.openBook(-1);
  }

  showIcon(rating:number) {
    return [].constructor( Math.floor(rating))
  }

  openBook(bookId: number){
    this.router.navigate(['/books', bookId]);
  }

  borrowBook(bookId: number){
    this.bookService.borrowBook(bookId, 4).subscribe((result: boolean) => { // todo
      this.dialog.open(DialogComponent, {
        width: '250px',
        data: {result: result},
      });
    })
  }

}

function search(books: Book[], text: string, page: number, pageSize: number): Book[] {
  return books.filter((book) => {
    const term = text.toLowerCase();
    return (
      book.author.toLowerCase().includes(term) ||
      book.name.toLowerCase().includes(term)
    );
  }).slice(
    (page - 1) * pageSize, (page - 1) * pageSize + pageSize,
  );
}

