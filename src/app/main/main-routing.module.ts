import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BookListComponent} from "./book-list/book-list.component";
import {MainComponent} from "./main.component";
import {UserListComponent} from "./user-list/user-list.component";
import {ReportsComponent} from "./reports/reports.component";
import {BorrowedBooksComponent} from "./borrowed-books/borrowed-books.component";
import {BookDetailComponent} from "./book-detail/book-detail.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'books',
        component: BookListComponent
      },
      {
        path: 'books/:id',
        component: BookDetailComponent
      },
      {
        path: 'borrowed',
        component: BorrowedBooksComponent
      },
      {
        path: 'reports',
        component: ReportsComponent
      },
      {
        path: 'users',
        component: UserListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
