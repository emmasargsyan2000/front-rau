import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable, startWith} from "rxjs";
import {Book, BookService, BorrowedBook} from "../services/book.service";
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {map} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-borrowed-books',
  templateUrl: './borrowed-books.component.html',
  styleUrls: ['./borrowed-books.component.css']
})
export class BorrowedBooksComponent {

  books$!: Observable<BorrowedBook[]>;
  books: BorrowedBook[] = [];
  collectionSize = this.books.length;
  filter = new FormControl('', { nonNullable: true });
  page = 1;
  pageSize = 5;
  today = new Date();

  dataSource!: MatTableDataSource<BorrowedBook>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  applyFilter(filterValue: any) {
    filterValue = filterValue.target.value.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  displayedColumns = [ 'name', 'author', 'category', 'startDate', 'endDate'];

  constructor(
    private router: Router,
    private bookService: BookService) {
    this.bookService.getUserBooks(5).subscribe((books: BorrowedBook[]) => { // todo
      this.books = books;
      this.collectionSize = books.length;
      this.dataSource = new MatTableDataSource(books);
      this.books$ = this.filter.valueChanges.pipe(
        startWith(''),
        map((text) => search(this.books, text, this.page, this.pageSize)),
      );
    });
  }

  refreshCountries() {
    this.books$ = this.filter.valueChanges.pipe(
      startWith(''),
      map((text) => search(this.books, text, this.page, this.pageSize)),
    );
  }

}

function search(books: BorrowedBook[], text: string, page: number, pageSize: number): BorrowedBook[] {
  return books.filter((book) => {
    const term = text.toLowerCase();
    return (
      book.author.toLowerCase().includes(term) ||
      book.name.toLowerCase().includes(term)||
      book.category.toLowerCase().includes(term)
    );
  }).slice(
    (page - 1) * pageSize, (page - 1) * pageSize + pageSize,
  );
}
