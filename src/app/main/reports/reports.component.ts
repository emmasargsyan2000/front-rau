import { Component } from '@angular/core';
import {ChartType} from "angular-google-charts";
import {ReportsService} from "../services/reports.service";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent {

  constructor(private reportsService: ReportsService) {
    this.reportsService.getNumOfBooksByFaculty().subscribe(booksByFaculty => this.booksByFaculty = booksByFaculty);
    this.reportsService.getNumOfBooksByGenre().subscribe(booksByGenre => this.booksByGenre = booksByGenre);
  }

  booksByGenreTitle = '# of books by Genre';
  booksByFacultyTitle = '# of books by Faculty';
  columnChart = ChartType.ColumnChart;
  pie = ChartType.PieChart;

  public booksByFaculty!: any[];
  public booksByGenre!: any[];

  optionsByFaculty = {};
  optionsByGenre = {
    legend: 'none'
  };
  width = 800;
  height = 700;

}
