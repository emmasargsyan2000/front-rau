import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import {RegistrationService} from "./registration.service";
import {RegistrationRoutingModule} from "./registration-routing.module";
import {MatGridListModule} from "@angular/material/grid-list";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    RegistrationComponent
  ],
  providers: [
    RegistrationService
  ],
  imports: [
    CommonModule,
    FormsModule,
    RegistrationRoutingModule,
    MatGridListModule
  ]
})
export class RegistrationModule { }
