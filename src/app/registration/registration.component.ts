import {AfterViewInit, Component, OnDestroy, OnInit} from "@angular/core";

@Component({
  moduleId: module.id,
  templateUrl: "registration.component.html",
  styleUrls: ["registration.component.css"]
})
export class RegistrationComponent {

  public validationMessage: string = '';

  constructor(
  ) {}

  public register(firstName: string, lastName: string, username: string, password: string): void {
  }

}
