import {Injectable} from '@angular/core';
import {Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate} from '@angular/router';
import {Observable, take} from 'rxjs';
import {AuthenticationService} from "../services/authentication.service";
import {map} from "rxjs/operators";
/**
 * This guards prevents user from navigating to
 * login page if he/she is already logged in.
 * */

@Injectable()
export class LoginPagePreventionGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return  this.authenticationService.isLoggedOn().pipe(map((isLogged: boolean) => {
        if (isLogged) {
          this.router.navigate(['/']);
          return false;
        }
        return true;
      }),
      take(1));
  }

}
