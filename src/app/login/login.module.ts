import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from "./login.component";
import {LoginRoutingModule} from "./login-routing.module";
import {LoginService} from "./login.service";
import {MatButtonModule} from "@angular/material/button";
import {MatGridListModule} from "@angular/material/grid-list";

@NgModule({
  declarations: [
    LoginComponent
  ],
  providers: [
    LoginService
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    MatButtonModule,
    MatGridListModule
  ]
})
export class LoginModule { }
