import {PlatformLocation} from '@angular/common';
export class RedirectUrlHelper {
  static getUrlAfterAppIdPart(platformLocation: PlatformLocation) {
    // returns application identifier, for example /indicata/
    const baseHref = platformLocation.getBaseHrefFromDOM();
    const origin = window.location.origin;
    const redirectUrl = window.location.href.substr(origin.length);
    // returns redirect url without application identifier
    return redirectUrl.substr(baseHref.length - 1);
  }
}
