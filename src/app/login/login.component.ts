import {Component} from "@angular/core";
import {LoginService} from "./login.service";

@Component({
  moduleId: module.id,
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {

  public validationMessage: string = '';

  constructor(
    protected loginService: LoginService,
  ) {}

  public login(username: string, password: string): void {
    this.loginService.loginOrGetErrorMessage(username, password).catch(() => {
      this.validationMessage = "Invalid data";
    });
  }

}
