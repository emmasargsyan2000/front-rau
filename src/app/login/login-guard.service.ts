import {Injectable} from '@angular/core';
import {Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanLoad, Route} from '@angular/router';
import {Observable, take} from 'rxjs';
import {RedirectUrlHelper} from './redirect-url-helper';
import {PlatformLocation} from '@angular/common';
import {AuthenticationService} from "../services/authentication.service";
import {map} from "rxjs/operators";
/**
 * Guard to check if requested user is logged
 */
@Injectable()
export class LoginGuard implements CanActivate, CanLoad {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private platformLocation: PlatformLocation
  ) {}

  /**
   * Check if user logged then return true otherwise navigate on login page
   * @returns true if user is logged otherwise false
   */

  canActivate(routeSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.authenticationService.redirectUrl = state.url;
    return this.authenticationService
      .isLoggedOn()
      .pipe(
        map((isLogged: boolean) => {
          if (!isLogged) {
            this.router.navigate(['/login']);
            return false;
          }
          return isLogged;
        }),
        take(1)
      );
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    this.authenticationService.redirectUrl = RedirectUrlHelper.getUrlAfterAppIdPart(this.platformLocation);
    return this.authenticationService
      .isLoggedOn()
      .pipe(
      map((isLogged: boolean) => {
        if (!isLogged) {
          this.router.navigate(['/login']);
          return false;
        }
        return isLogged;
      }),
      take(1));
  }

}
