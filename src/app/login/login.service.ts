import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import {map} from "rxjs/operators";

@Injectable()
export class LoginService {

  private static readonly REGISTRATION_URL = '/registration';

  // Should not be subscribed inside of service, this should be fixed later
  private _loginRequestState: Promise<Object> | null = null;

  constructor(
    protected router: Router,
    protected authenticationService: AuthenticationService,
  ) {
  }

  public login(username: string, password: string): Promise<boolean> {
    return this.loginOrGetErrorMessage(username, password)
      .then(() => true)
      .catch(() => false);
  }

  public loginOrGetErrorMessage(username: string, password: string): Promise<Object> {
    if (this._loginRequestState === null) {
      this._loginRequestState = new Promise((resolve, reject) => {
        this.authenticationService
          .login(username, password).pipe(
            map((login: any) => {
            switch (login.loginResponse) {
              case 0:
               this.router.navigateByUrl(this.authenticationService.redirectUrl).then(() => {
                 this.resetStateAndResolve(resolve);
               });
               break;
              default:
                this.rejectWithMessage(reject);
            }
          }))
          .toPromise()
          .catch( () => {
            this.rejectWithMessage(reject);
          });
      });
    }
    return this._loginRequestState;
  }

  // public verify(code: string, verificationType: VerificationType, switchAccount?: boolean, username?: string): Promise<Object> {
  //   if (this._loginRequestState === null) {
  //     this._loginRequestState = new Promise((resolve, reject) => {
  //       if (!code) {
  //         this.rejectWithMessage(reject, LoginStatus.VERIFICATION_CODE_INCORRECT, LoginService.VERIFICATION_CODE_INCORRECT_MESSAGE);
  //         return;
  //       }
  //       this.authenticationService
  //         .verifyCode(code, verificationType, username)
  //         .map((verified: boolean) => {
  //           if (verified) {
  //             this.accountService.refreshCurrent(switchAccount).then(() => {
  //               this.router.navigate([this.authenticationService.redirectUrl]).then(() => {
  //                 this.resetStateAndResolve(resolve);
  //               });
  //             });
  //           } else {
  //             this.rejectWithMessage(reject, LoginStatus.VERIFICATION_CODE_INCORRECT, LoginService.VERIFICATION_CODE_INCORRECT_MESSAGE);
  //           }
  //         })
  //         .toPromise()
  //         .catch((error: any) => {
  //           this.handleError(error);
  //           this.rejectWithMessage(reject, LoginStatus.FAILED, error.messageId, error.placeholders);
  //         });
  //     });
  //   }
  //   return this._loginRequestState;
  // }

  public registration() {
    this.router.navigateByUrl(LoginService.REGISTRATION_URL);
  }

  public logout(): void {
    this.authenticationService.logout(true);
  }

  private resetLoginRequestState() {
    this._loginRequestState = null;
  }

  private resetStateAndResolve(resolve: Function) {
    this.resetLoginRequestState();
    resolve({loginStatus: "Done", message: null, data: null});
  }

  private rejectWithMessage(reject: Function) {
    this.resetLoginRequestState();
    reject({message: "Login Failed", data: null});
  }

}
